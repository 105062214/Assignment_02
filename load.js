var loadState = {
    preload: function() {
        game.load.image('bigbackground', 'assets/bkg_big.png');
        game.load.image('smallbackground', 'assets/bkg_small.jpg');
        game.load.image('sidebar', 'assets/sidebar.jpg');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.image('normal', 'assets/normal.png');
        game.load.image('nails', 'assets/nails.png');
        game.load.image('blood', 'assets/blood.png');
        game.load.image('enemyBlood', 'assets/enemyBlood.png');
        game.load.image('llama', 'assets/llama.png');
        game.load.image('llamaEffect', 'assets/llamaEffect.png');
        game.load.image('crosshair', 'assets/crosshair.png');
        game.load.image('bullet', 'assets/bullet.png');
        game.load.image('enemy', 'assets/enemy.png');
        game.load.spritesheet('player', 'assets/player.png', 32, 32);
        game.load.spritesheet('cvyL', 'assets/conveyor_left.png', 124, 21);
        game.load.spritesheet('cvyR', 'assets/conveyor_right.png', 124, 21);
        game.load.spritesheet('fake', 'assets/fake.png', 126, 47);
        game.load.spritesheet('trampoline', 'assets/trampoline.png', 126, 29);
        game.load.spritesheet('enemyHP', 'assets/enemyHP.png', 10, 40);

        game.load.audio('damage', ['assets/damage.mp3', 'assets/damage.ogg']);
        game.load.audio('spring', ['assets/Spring.mp3', 'assets/Spring.ogg']);
        game.load.audio('coin', ['assets/coin.mp3', 'assets/coin.ogg']);
        game.load.audio('hit', ['assets/hit.mp3', 'assets/hit.ogg']);
        
        
    },
    create: function() {
        game.state.start('menu');
    }
}