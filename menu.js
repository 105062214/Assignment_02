

var menuState = {
    preload: function(){
        /*game.load.image('bigbackground', 'assets/bkg_big.png');
        game.load.image('smallbackground', 'assets/bkg_small.jpg');
        game.load.image('sidebar', 'assets/sidebar.jpg');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.spritesheet('player', 'assets/player.png', 32, 32);*/

    },
    create: function(){
        game.renderer.renderSession.roundPixels = true;
        game.canvas.style.cursor = "default";    

        this.cursor = game.input.keyboard.createCursorKeys();
        this.space = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        game.add.sprite(20, 35, 'smallbackground');

        this.sidebarL = game.add.sprite(20, 35, 'sidebar', 0, this.sidebars);
        this.sidebarR = game.add.sprite(600, 35, 'sidebar', 0, this.sidebars);  

        this.ceiling = game.add.sprite(40, 35, 'ceiling');
        this.player = game.add.sprite(320, 250, 'player');
        this.player.anchor.setTo(0.5, 0.5);
        this.player.frame = 8;

        game.add.image(0, 0, 'bigbackground');

        this.startLabel = game.add.text(200, 350, 'Press SPACE to start the game', {font: '20px Arial', fill: '#FFFFFF'});
        this.tutorialLabel = game.add.text(70, 380, 'Use A and D to control the player!   Use Mouse to aim and Left Mouse Button to shoot!', {font: '13px Arial', fill: '#FFFFFF'});
        
        this.startLabelTween = game.add.tween(this.startLabel);
        this.startLabelTween.to({y: this.startLabel.y-10}, 100).yoyo(true);
        game.time.events.loop(1500, this.bounceStartLabel, this);

        ///ScoreLabel
        this.scoreLabel = game.add.text(700, 80, 'score: 0', {font: '30px Arial', fill: '#333333'});
        ///
        ///hpLabel
        this.hpLabel = game.add.text(700, 140, 'HP: 10', {font: '30px Arial', fill: '#333333'});
        ///

        //Rank
        var top5_name = ['player1', 'player2', 'player3', 'player4', 'player5'];
        var top5_score = [0, 0, 0, 0, 0];
        var recordRef = firebase.database().ref('records');

        recordRef.once('value')
            .then(function (snapshot){
                snapshot.forEach(function (childSnapshot){
                    childData = childSnapshot.val();
                    
                    for(var i=0; i<5 ; i++){
                        if(childData.score >= top5_score[i]){
                            for(var j=4; j>i; j--){
                                top5_score[j] = top5_score[j-1];
                                top5_name[j] = top5_name[j-1];
                            }
                            top5_score[i] = childData.score;
                            if(childData.username.length > 15)
                                top5_name[i] = childData.username.substr(0,14) + '...';
                            else
                                top5_name[i] = childData.username;
                            break;
                        }
                    }
                });  
                this.rankLabelName0 = game.add.text(680, 250, 'HIGHEST SCORE', {font: '16px Arial', fill: '#333333'})
                this.rankLabelName0 = game.add.text(650, 290, top5_name[0], {font: '20px Arial', fill: '#333333'})
                this.rankLabelName1 = game.add.text(650, 320, top5_name[1], {font: '20px Arial', fill: '#333333'})
                this.rankLabelName2 = game.add.text(650, 350, top5_name[2], {font: '20px Arial', fill: '#333333'})
                this.rankLabelName3 = game.add.text(650, 380, top5_name[3], {font: '20px Arial', fill: '#333333'})
                this.rankLabelName4 = game.add.text(650, 410, top5_name[4], {font: '20px Arial', fill: '#333333'})

                this.rankLabelScore0 = game.add.text(800, 290, top5_score[0], {font: '20px Arial', fill: '#333333'})
                this.rankLabelScore1 = game.add.text(800, 320, top5_score[1], {font: '20px Arial', fill: '#333333'})
                this.rankLabelScore2 = game.add.text(800, 350, top5_score[2], {font: '20px Arial', fill: '#333333'})
                this.rankLabelScore3 = game.add.text(800, 380, top5_score[3], {font: '20px Arial', fill: '#333333'})
                this.rankLabelScore4 = game.add.text(800, 410, top5_score[4], {font: '20px Arial', fill: '#333333'})
            })
            .catch(e => console.log(e.message));

        
        
        //
    },
    update: function(){
        if(this.space.isDown)
            game.state.start('play');
    },
    bounceStartLabel: function(){
        //this.startLabelTween.easing(Phaser.Easing.Bounce.Out);
        this.startLabelTween.start();
    }

}