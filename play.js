var playState = {
    distance: 0,
    preload: function() {

        // Loat game sprites.
        
        

        /// Load block spritesheet.
        /*game.load.spritesheet('block1', 'assets/block1.png', 28, 28);
        game.load.spritesheet('block2', 'assets/block2.png', 28, 28);*/
        

        /// ToDo: Load spritesheet
        ///      The name of sprite is 'player'.
        ///      The spritesheet filename is assets/MARIO.png
        ///      A frame size is 32 x 54.
        
        ///

    },
    create: function() {
        
        
        this.score = 1;
        this.lose = false;
        this.recorded = false;
        this.lastCrosshair;
        this.fireRateTime = 200;
        this.nextFireTime = 0;

        game.physics.startSystem(Phaser.Physics.ARCADE);
        


        this.cursor = game.input.keyboard.createCursorKeys();
        this.wasd = {A:game.input.keyboard.addKey(Phaser.Keyboard.A), D:game.input.keyboard.addKey(Phaser.Keyboard.D)};
        
        
        this.smallbkg = game.add.sprite(20, 35, 'smallbackground');
        
        /*game.input.addMoveCallback(function(point, x, y){
            this.crosshair.x = x;
            this.crosshair.y = y;
        });*/

        this.smallbkg.inputEnabled = true;
        this.smallbkg.events.onInputOver.add(function(){
            game.canvas.style.cursor = "none";
            
        }, this);
        this.smallbkg.events.onInputOut.add(function(){
            game.canvas.style.cursor = "default";    
        }, this);

        
        
        
        
        ///Items
        this.items = game.add.group();
        this.items.enableBody = true;
        ///

        /// Platforms
        this.platforms = game.add.group();
        this.platforms.enableBody = true;
        ///

        this.ceiling = game.add.sprite(40, 35, 'ceiling');
        game.physics.arcade.enable(this.ceiling);
        this.ceiling.enableBody = true;
        this.ceiling.body.immovable = true;

        this.player = game.add.sprite(320, 250, 'player');
        game.physics.arcade.enable(this.player);
        this.player.anchor.setTo(0.5, 0.5);
        this.player.enableBody = true;
        this.player.body.gravity.y = 750;

        this.player.animations.add('leftwalk', [0, 1, 2, 3], 8, true);
        this.player.animations.add('rightwalk', [9, 10, 11, 12], 8, true);
        this.player.animations.add('leftfly', [18, 19, 20, 21], 12, true);
        this.player.animations.add('rightfly', [27, 28, 29, 30], 12, true);
        this.player.animations.add('upfly', [36, 37, 38, 39], 12, true);
        this.player.stand = undefined;
        this.player.lastCeilingTime = 0;
        this.player.lastBounceTime = 0;
        this.player.lastBounceOn = undefined;
        this.player.hp = 10;
        
        ///bullet
        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        
        ///

        ///Enemies
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.hpBar = game.add.group();
        ///

        ///sidebars
        this.sidebars = game.add.group();
        this.sidebars.enableBody = true;
        this.sidebarL = game.add.sprite(20, 35, 'sidebar', 0, this.sidebars);
        this.sidebarR = game.add.sprite(600, 35, 'sidebar', 0, this.sidebars);  
        this.sidebars.setAll('body.immovable', true);
        ///

        ///BigBKG
        this.bigbkg = game.add.image(0, 0, 'bigbackground');
        this.bigbkg.enableBody = true;
        
        ///

        ///ScoreLabel
        this.scoreLabel = game.add.text(700, 80, 'score: 1', {font: '30px Arial', fill: '#333333'});
        ///
        ///hpLabel
        this.hpLabel = game.add.text(700, 140, 'HP: 10', {font: '30px Arial', fill: '#333333'});
        ///

        ///BloodEffect
        this.bloodEmitter = game.add.emitter(0, 0, 45);
        this.bloodEmitter.makeParticles('blood');
        this.bloodEmitter.setYSpeed(-175, 250);
        this.bloodEmitter.setXSpeed(-150, 150);
        this.bloodEmitter.setScale(2, 0, 2, 0, 400);
        this.bloodEmitter.gravity = 0;
        ///

        ///HitEffect
        this.hitEmmiter = game.add.emitter(0, 0, 15);
        this.hitEmmiter.makeParticles('enemyBlood');
        this.hitEmmiter.setYSpeed(-75, 170);
        this.hitEmmiter.setXSpeed(-75, 75);
        this.hitEmmiter.setScale(3, 1, 3, 1, 600);
        this.hitEmmiter.gravity = 0;
        ///
        ///Sound
        this.damageSound = game.add.audio('damage');
        this.damageSound.volume = 0.8;
        this.springSound = game.add.audio('spring');
        this.coinSound = game.add.audio('coin');
        this.coinSound.volume = 0.8;
        this.hitSound = game.add.audio('hit');
        ///
        ///Rank
        var top5_name = ['player1', 'player2', 'player3', 'player4', 'player5'];
        var top5_score = [0, 0, 0, 0, 0];
        var recordRef = firebase.database().ref('records');

        recordRef.once('value')
            .then(function (snapshot){
                snapshot.forEach(function (childSnapshot){
                    childData = childSnapshot.val();
                    
                    for(var i=0; i<5 ; i++){
                        if(childData.score >= top5_score[i]){
                            for(var j=4; j>i; j--){
                                top5_score[j] = top5_score[j-1];
                                top5_name[j] = top5_name[j-1];
                            }
                            top5_score[i] = childData.score;
                            if(childData.username.length > 15)
                                top5_name[i] = childData.username.substr(0,14) + '...';
                            else
                                top5_name[i] = childData.username;
                            break;
                        }
                    }
                });  
                this.rankLabelName0 = game.add.text(680, 250, 'HIGHEST SCORE', {font: '16px Arial', fill: '#333333'})
                this.rankLabelName0 = game.add.text(650, 290, top5_name[0], {font: '20px Arial', fill: '#333333'})
                this.rankLabelName1 = game.add.text(650, 320, top5_name[1], {font: '20px Arial', fill: '#333333'})
                this.rankLabelName2 = game.add.text(650, 350, top5_name[2], {font: '20px Arial', fill: '#333333'})
                this.rankLabelName3 = game.add.text(650, 380, top5_name[3], {font: '20px Arial', fill: '#333333'})
                this.rankLabelName4 = game.add.text(650, 410, top5_name[4], {font: '20px Arial', fill: '#333333'})

                this.rankLabelScore0 = game.add.text(800, 290, top5_score[0], {font: '20px Arial', fill: '#333333'})
                this.rankLabelScore1 = game.add.text(800, 320, top5_score[1], {font: '20px Arial', fill: '#333333'})
                this.rankLabelScore2 = game.add.text(800, 350, top5_score[2], {font: '20px Arial', fill: '#333333'})
                this.rankLabelScore3 = game.add.text(800, 380, top5_score[3], {font: '20px Arial', fill: '#333333'})
                this.rankLabelScore4 = game.add.text(800, 410, top5_score[4], {font: '20px Arial', fill: '#333333'})
            })
            .catch(e => console.log(e.message));

        ///

        this.createFirstPlatform();
        game.time.events.loop(750, this.createOnePlatform, this);
        game.time.events.loop(2500, this.createEnemy, this);
        game.time.events.loop(2400, this.addScore, this);
        

        /*this.player = game.add.sprite(game.width/2, game.height/2, 'player');
        this.player.facingLeft = false;


        /// ToDo: Add 4 animations.
        /// 1. Create the 'rightwalk' animation  by looping the frames 1 and 2

        /// 2. Create the 'leftwalk' animation by looping the frames 3 and 4
        
        /// 3. Create the 'rightjump' animation (frames 5 and 6 and no loop)
        
        /// 4. Create the 'leftjump' animation (frames 7 and 8 and no loop)
        
        ///


        /// Add a little yellow block :)
        this.yellowBlock = game.add.sprite(200, 320, 'block1');
        this.yellowBlock.animations.add('Yblockanim', [0, 1, 2, 3], 8,  true);
        game.physics.arcade.enable(this.yellowBlock);
        this.yellowBlock.body.immovable = true;        
        
        /// Add a little dark blue block ;)
        this.blueBlock = game.add.sprite(422, 320, 'block2');
        this.blueBlock.animations.add('Bblockanim', [0, 1, 2, 3], 8,  true);
        game.physics.arcade.enable(this.blueBlock);
        this.blueBlock.body.immovable = true;
        

        /// Particle
        this.emitter = game.add.emitter(422, 320, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 500;


        /// Add floor
        this.floor = game.add.sprite(0, game.height - 30, 'ground'); 
        game.physics.arcade.enable(this.floor);
        this.floor.body.immovable = true;

        game.physics.arcade.enable(this.player);

        // Add vertical gravity to the player
        this.player.body.gravity.y = 500;

    },
    blockTween: function() {

        /// ToDo: Tween yellow block.
        ///      Move block to 20px above its original place and move it back (yoyo function).
        var yellowBlockOriginalX = this.yellowBlock.x;
        var yellowBlockOriginalY = this.yellowBlock.y;
        /// Add Tween here: game.add.tween(this.yellowBlock)....? 

        ///
    },

    blockParticle: function() {

        /// ToDo: Start our emitter.
        ///      1. We'll burst out all partice at once.
        ///      2. The particle's lifespan is 800 ms.
        ///      3. Set frequency to null since we will burst out all partice at once.
        ///      4. We'll launch 15 particle.

        ///*/
    },
    update: function() {
        /*game.physics.arcade.collide(this.player, this.walls);
        game.physics.arcade.collide(this.player, this.floor);
        game.physics.arcade.collide(this.player, this.yellowBlock, this.blockTween, null, this);
        game.physics.arcade.collide(this.player, this.blueBlock, this.blockParticle, null, this);

        // Play the animation.
        this.yellowBlock.animations.play('Yblockanim');
        this.blueBlock.animations.play('Bblockanim');


        if (!this.player.inWorld) { this.playerDie();}
        this.movePlayer();
    }, 
    playerDie: function() { game.state.start('main');},

    /// ToDo: Finish the 4 animation part.
    movePlayer: function() {
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -200;
            this.player.facingLeft = true;

            /// 1. Play the animation 'leftwalk'

            ///
        }
        else if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 200;
            this.player.facingLeft = false;

            /// 2. Play the animation 'rightwalk' 

            ///
        }    

        // If the up arrow key is pressed, And the player is on the ground.
        else if (this.cursor.up.isDown) { 
            if(this.player.body.touching.down){
                // Move the player upward (jump)
                if(this.player.facingLeft) {
                    /// 3. Play the 'leftjump' animation

                    ///
                }else {
                    /// 4. Play the 'rightjump' animation

                    ///
                }
                this.player.body.velocity.y = -350;
            }
        }  
        // If neither the right or left arrow key is pressed
        else {
            // Stop the player 
            this.player.body.velocity.x = 0;
        
            if(this.player.facingLeft) {
                // Change player frame to 3 (Facing left)
                this.player.frame = 3;
            }else {
                // Change player frame to 1 (Facing right)
                this.player.frame = 1;
            }

            // Stop the animation
            this.player.animations.stop();
        } */  
        game.physics.arcade.collide(this.player, this.sidebars);
        game.physics.arcade.collide(this.player, this.platforms, this.platformAction, null, this);
        game.physics.arcade.overlap(this.player, this.ceiling, this.ceilingDamaging, null, this);
        game.physics.arcade.overlap(this.player, this.items, this.itemAction, null, this);
        game.physics.arcade.overlap(this.bullets, this.bigbkg, this.killBullet, null, this);
        game.physics.arcade.overlap(this.bullets, this.sidebars, this.killBullet, null, this);
        game.physics.arcade.collide(this.bullets, this.enemies, this.hitEnemy, null, this);
        game.physics.arcade.overlap(this.player, this.enemies, this.enemyDamaging, null, this);
        this.movePlayer();
        this.trackMouse();
        this.moveEnemies(this.player);
        this.movePlatforms();
        this.moveItems();
        this.updateHP();
        this.checkLose();
        
    },
    movePlayer: function() {
        if(this.lose)
            return;
        if(this.cursor.left.isDown || this.wasd.A.isDown){
            this.player.body.velocity.x = -270;
            if(this.player.body.touching.down){
                this.player.animations.play('leftwalk');
            }else{
                this.player.animations.play('leftfly');
            }
            
        }
        else if(this.cursor.right.isDown || this.wasd.D.isDown){
            this.player.body.velocity.x = 270;
            if(this.player.body.touching.down){
                this.player.animations.play('rightwalk');
            }else{
                this.player.animations.play('rightfly');
            }
        }else{
            this.player.body.velocity.x = 0;
            if(this.player.body.touching.down){
                this.player.frame = 8;
                this.player.animations.stop();
            }else{
                this.player.animations.play('upfly');
            }
            
        }
    },
    trackMouse: function(){
        if(this.lastCrosshair != undefined)
            this.lastCrosshair.kill();
        var crosshair = game.add.sprite(-40, -40, 'crosshair');
        crosshair.anchor.set(0.5,0.5);
        if(game.input.x > 20 && game.input.x < 620 && game.input.y > 35 && game.input.y < 485){
            crosshair.x = game.input.x;
            crosshair.y = game.input.y;
            if(game.input.activePointer.isDown)
                this.fire();
        }else{
            crosshair.x = -40;
            crosshair.y = -40;
        }
        this.lastCrosshair = crosshair;  
    },
    fire: function(){
        if (game.time.now > this.nextFireTime){
            this.nextFireTime = game.time.now + this.fireRateTime;
            var bullet = game.add.sprite(this.player.x, this.player.y, 'bullet', 0, this.bullets);
            bullet.anchor.set(0.5,0.5);
            game.physics.arcade.moveToPointer(bullet, 500);
        }
    },
    killBullet: function(bullet, bound){
        bullet.kill();
    },
    createEnemy: function(){
        var enemyGenPosition = [{x: 0, y:100}, {x: 0, y: 240}, {x: 0, y: 400}, {x: 660, y:100}, {x: 660, y: 240}, {x: 660, y: 400} ];
        var newEnemyPosition = game.rnd.pick(enemyGenPosition);
        var enemy = game.add.sprite(newEnemyPosition.x, newEnemyPosition.y, 'enemy', 0, this.enemies);
        enemy.anchor.set(0.5, 0.5);
        enemy.hp = 3;
        enemy.hpBar = game.add.sprite(newEnemyPosition.x - 30, newEnemyPosition.y, 'enemyHP', 0, this.enemies.hpBar);
        enemy.hpBar.anchor.set(0.5, 0.5);
        enemy.hpBar.frame = 0;
        
    },
    moveEnemies: function(player){
        this.enemies.forEach( function(eachEnemy){
            var ratioX = (player.x - eachEnemy.x ) / Math.abs(player.y - eachEnemy.y);
            var ratioY = (player.y - eachEnemy.y) / Math.abs(player.y - eachEnemy.y);
            var speed = 30;
            var parameter = Math.sqrt(Math.pow(speed, 2)/ (Math.pow(ratioX, 2) + 1));
            eachEnemy.body.velocity.x = Math.round( ratioX * parameter);
            eachEnemy.body.velocity.y = Math.round( ratioY * parameter);
            if( (player.x - eachEnemy.x) < 0){
                eachEnemy.scale.set(-1, 1);
                eachEnemy.hpBar.x = eachEnemy.x + 30;
                eachEnemy.hpBar.y = eachEnemy.y;
            }else{
                eachEnemy.scale.set(1, 1);
                eachEnemy.hpBar.x = eachEnemy.x - 30;
                eachEnemy.hpBar.y = eachEnemy.y;
            }
        });
    },
    hitEnemy: function(bullet, enemy){
        bullet.kill();
        this.hitSound.play();
        this.hitEmmiter.x = (bullet.x + enemy.x) / 2;
        this.hitEmmiter.y = (bullet.y + enemy.y) / 2;
        if(bullet.x < enemy.x)
            this.hitEmmiter.setXSpeed(-150, 40);
        else
            this.hitEmmiter.setXSpeed(-40, 150);
        this.hitEmmiter.start(true, 400, null, 15);
        if(enemy.hp > 1) {
            enemy.hp--;
            enemy.hpBar.frame = 3 - enemy.hp;
        }
        else{
            enemy.hpBar.kill();
            enemy.kill();
        }
            
    },
    enemyDamaging: function(player, enemy){
        game.camera.flash(0xe60000, 150);
        game.camera.shake(0.01, 150);
        player.hp -= 3;
        this.bloodEmitter.x = this.player.x;
        this.bloodEmitter.y = this.player.y;
        this.bloodEmitter.start(true, 800, null, 45);
        this.damageSound.play();
        enemy.kill();
        enemy.hpBar.kill();
        
    },
    createFirstPlatform: function(){
        
        var platform;
        var x = 255;
        var y = 500;
        platform = game.add.sprite(x, y, 'normal', 0, this.platforms);
        platform.body.immovable = true;
        platform.body.checkCollision.down = false;
        platform.body.checkCollision.right = false;
        platform.body.checkCollision.left = false;
        this.player.lastBounceOn = platform;
    },
    createOnePlatform: function(){
        
        var platform;
        var x = Math.random() * (600 - 40 - 200 ) + 75;
        var y = 500;
        var platformType = ['normal', 'normal', 'nails', 'cvyL', 'cvyR', 'fake', 'trampoline' ];
        var newPlatformType = game.rnd.pick(platformType);
        if(newPlatformType == 'nails' || newPlatformType == 'fake'){
            y -= 13;
        }else if(newPlatformType == 'trampoline'){
            y += 3;
        }
        platform = game.add.sprite(x, y, newPlatformType, 0, this.platforms);
        if(newPlatformType == 'nails'){
            platform.body.setSize(124, 18, 0, 22);
        }
        else if(newPlatformType == 'cvyL'){
            platform.animations.add('scroll', [0, 1, 2, 3], 8, true);
            platform.animations.play('scroll');
        }else if(newPlatformType == 'cvyR'){
            platform.animations.add('scroll', [0, 1, 2, 3], 8, true);
            platform.animations.play('scroll');
        }else if(newPlatformType == 'fake'){
            platform.animations.add('flip', [0, 1, 2, 3, 4, 5, 0], 6, false);
            platform.body.setSize(126, 20, 0, 12);
        }else if(newPlatformType == 'trampoline'){
            platform.animations.add('bounce', [0, 1, 2, 3, 4, 5, 4, 3, 2, 1, 0, 1, 2, 3, 2, 1, 0], 36, false);
        }

        var item;
        var itemType = ['llama'];
        
        var probabilityLlama = Math.random() * 100;
        if(probabilityLlama > 97){
            var newItemType = game.rnd.pick(itemType);
            item = game.add.sprite(x+50, y-65, newItemType, 0, this.items);
            item.anchor.set(0.5,0.5);
            item.used = false;
        }
        platform.jumped = false;
        platform.body.immovable = true;
        platform.body.checkCollision.down = false;
        platform.body.checkCollision.right = false;
        platform.body.checkCollision.left = false;
    },
    movePlatforms: function(){
        this.platforms.setAll('body.velocity.y', -100);
        this.platforms.children.forEach(element => {
            if(element.body.position.y < 67){
                element.body.checkCollision.up = false;
            }
            else if (element.body.position.y < 5){
                element.kill();
            }
        });
    },
    moveItems: function(){
        this.items.setAll('body.velocity.y', -100);
    },
    platformAction: function(player, platform){
        
        
        if(platform.key == 'nails' && player.stand != platform && platform.jumped == false){
            game.camera.flash(0xe60000, 150);
            this.bloodEmitter.x = this.player.x;
            this.bloodEmitter.y = this.player.y;
            this.bloodEmitter.start(true, 800, null, 45);
            this.damageSound.play();
            player.hp -= 5;
            player.stand = platform;
            if(platform != player.lastBounceOn ){
                player.lastBounceOn.jumped = true;
            }
            player.lastBounceOn = platform;
        }else if(platform.key == 'cvyL' && platform.jumped == false){
            if(player.hp < 10 && player.stand != platform)
                player.hp += 1;
            player.body.x -= 2;
            player.stand = platform;
            if(platform != player.lastBounceOn ){
                player.lastBounceOn.jumped = true;
            }
            player.lastBounceOn = platform;
        }else if(platform.key == 'cvyR' && platform.jumped == false){
            if(player.hp < 10 && player.stand != platform)
                player.hp += 1;
            player.body.x += 2;
            player.stand = platform;
            if(platform != player.lastBounceOn ){
                player.lastBounceOn.jumped = true;
            }
            player.lastBounceOn = platform;
        }else if(platform.key == 'fake' && platform.jumped == false){
            if(player.hp < 10 && player.stand != platform)
                player.hp += 1;
            platform.animations.play('flip');
            platform.body.checkCollision.up = false;
            player.stand = platform;
            if(platform != player.lastBounceOn ){
                player.lastBounceOn.jumped = true;
            }
            player.lastBounceOn = platform;
        }else if(platform.key == 'trampoline' && game.time.now > player.lastBounceTime + 400 && platform.jumped == false){
            if(player.hp < 10 && game.time.now > player.lastCeilingTime + 200)
                player.hp += 1;
            platform.animations.play('bounce');
            player.body.velocity.y = -350;
            this.springSound.play();
            player.stand = platform;
            player.lastBounceTime = game.time.now;
            if(platform != player.lastBounceOn ){
                player.lastBounceOn.jumped = true;
            }
            player.lastBounceOn = platform;
            
        }else if(platform.key == 'normal' && platform.jumped == false){
            if(player.hp < 10 && player.stand != platform)
                player.hp += 1;
            player.stand = platform;
            if(platform != player.lastBounceOn ){
                player.lastBounceOn.jumped = true;
            }
            player.lastBounceOn = platform;
        }
        

        
    },
    itemAction: function(player, item){
        
        if(item.key == 'llama'){
            var tweenScale = game.add.tween(item.scale);
            var tweenAlpha = game.add.tween(item);
            
            tweenScale.to({x: 2, y: 2}, 250).start();
            tweenAlpha.to({alpha: 0}, 250).start();
            
            if(item.used == false){
                this.score += 10;
                player.hp = 10;
                item.used = true;
                this.coinSound.play();
                var llamaEffect = game.add.sprite(item.x-50, item.y-50, 'llamaEffect');
                llamaEffect.alpha = 0;
                var tweenEffect = game.add.tween(llamaEffect);
                var tweenEffect2 = game.add.tween(llamaEffect);
                tweenEffect.to({alpha: 0.7}, 1000).yoyo(true).start();
                tweenEffect2.to({y: llamaEffect.y-120}, 2000).start();
                
            }
            this.scoreLabel.text = 'score: ' + this.score.toString();
        }
        game.time.events.add(400, function(){
            item.kill();
        })
    },
    ceilingDamaging: function(){
        if(this.player.body.velocity.y < 0){
            this.player.body.velocity.y = 0;
        }
        if(game.time.now > this.player.lastCeilingTime){
            game.camera.flash(0xe60000, 150);
            this.bloodEmitter.x = this.player.x;
            this.bloodEmitter.y = this.player.y;
            this.bloodEmitter.start(true, 800, null, 45);
            this.damageSound.play();
            this.player.hp -= 5;
            this.player.lastCeilingTime = game.time.now + 1000
        }

    },
    checkLose: function(){
        if(this.player.hp <= 0 || this.player.body.y > 510){
            this.lose = true;
            this.loseLabel = game.add.text(240, 150, 'YOU LOSE', {font: '30px Arial', fill: '#FFFFFF'});

            
            game.time.events.add(2000, function(){
                
                var user_name = prompt("Please enter your name.", "16歲小朋友");
                if(user_name != null){
                    var newRecordRef = firebase.database().ref('records/').push();
                    newRecordRef.set({
                        username: user_name,
                        score: playState.score
                    });
                }
                
                game.state.start('menu');
            })
            
        }
    },
    addScore: function(){
        if(this.lose == false){
            this.score++;
            this.scoreLabel.text = 'score: ' + this.score.toString();
        }
    },
    updateHP: function(){
        if(this.lose == false)
            this.hpLabel.text = 'HP: ' + this.player.hp.toString();
        else{
            this.hpLabel.text = 'HP: 0';
        }
    }
};




