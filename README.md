# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>


## Scoring 
    |                                              Item                                              | Score | Done |
    |:----------------------------------------------------------------------------------------------:|:-----:|:----:|
    | A complete game process: start menu => game view => game over => quit or play again            |  20%  | checked |  
    | Your game should follow the basic rules of  "小朋友下樓梯".                                     |  15%  | checked |  
    |         All things in your game should have correct physical properties and behaviors.         |  15%  | checked |  
    | Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  | checked |  
    | Add some additional sound effects and UI to enrich your game.                                  |  10%  | checked |  
    | Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  | checked |  
    | Appearance (subjective)                                                                        |  10%  |  ?   |  
    | Other creative features in your game (describe on README.md)                                   |  10%  | checked |

## 基本的功能
仿照原版小朋友下樓梯的多種平台
踩到特殊平台會有特殊的音效
受傷的時候會噴血跟螢幕閃爍
並且可以將排行榜資料跟雲端作互動

## Other features
多了射擊模式:
玩家可以用滑鼠進行瞄準 並發射子彈 
遊戲會定時產生豬頭怪物
玩家必須射擊豬頭以避免被攻擊 

多一個特殊道具:
有5%的機率在產生平台的時候會產生一個llama
吃到他可以增加10分 並且補滿血量

